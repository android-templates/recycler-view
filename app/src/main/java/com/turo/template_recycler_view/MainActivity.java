package com.turo.template_recycler_view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

public class MainActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.main_recycler_view);
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*
        Quote about LayoutManager from developer.android.com:

        "In contrast to other adapter-backed views such as ListView or GridView, RecyclerView allows
        client code to provide custom layout arrangements for child views. These arrangements are
        controlled by the RecyclerView.LayoutManager. A LayoutManager must be provided for RecyclerView
        to function."
        */
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        try {
            // A dummy JSONArray that contains fake data about LoL Champions.
            JSONArray elements = new JSONArray(
                    "[{\"first_line\":\"Morgana\",\"second_line\":\"Fallen Angel\"}," +
                    "{\"first_line\":\"Lux\",\"second_line\":\"The Lady of Luminosity\"}," +
                    "{\"first_line\":\"Ahri\",\"second_line\":\"The Nine-Tailed Fox\"}," +
                    "{\"first_line\":\"Lucian\",\"second_line\":\"The Purifier\"}," +
                    "{\"first_line\":\"Gragas\",\"second_line\":\"The Rabble Rouser\"}," +
                    "{\"first_line\":\"Jinx\",\"second_line\":\"The Loose Cannon\"}," +
                    "{\"first_line\":\"Vi\",\"second_line\":\"The Piltover Enforcer\"}," +
                    "{\"first_line\":\"Caitlyn\",\"second_line\":\"The Sheriff of Piltover\"}," +
                    "{\"first_line\":\"Pantheon\",\"second_line\":\"The Artisan of War\"}," +
                    "{\"first_line\":\"Lulu\",\"second_line\":\"The Fae Sorceress\"}," +
                    "{\"first_line\":\"Malphite\",\"second_line\":\"Shard of the Monolith\"}," +
                    "{\"first_line\":\"Olaf\",\"second_line\":\"The Berserker\"}," +
                    "{\"first_line\":\"Volibear\",\"second_line\":\"The Thunder's Roar\"}]"
            );
            Log.d(TAG, "onResume - elements: " + elements.toString());

            /*
            Quote from developer.android.com:
            "Adapters provide a binding from an app-specific data set to views that
            are displayed within a RecyclerView."
            */
            mAdapter = new MyAdapter(elements,this);
            // Applies the Adapter that will be used on the Recycler View
            mRecyclerView.setAdapter(mAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
